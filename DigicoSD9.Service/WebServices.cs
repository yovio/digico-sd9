﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using NLog;

namespace DigicoSD9.Service
{
    public class WebServices
    {
        Logger _logger = LogManager.GetCurrentClassLogger();
        private IDisposable _signalRApp;

        #region Public Methods
        public void Start()
        {
            string signalRUrl = "http://*:8080";
            _signalRApp = WebApp.Start<OwinStartup>(signalRUrl);
            _logger.Info("SignalR WebServices started on {0}", signalRUrl);
        }

        public void Stop()
        {
            if(_signalRApp != null)
                _signalRApp.Dispose();
        }
        #endregion
    }
}
