﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DigicoSD9.Model;
using DigicoSD9.Service.SignalR.Hubs;
using DigicoSD9.Utility;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;
using NLog;
using Rug.Osc;

namespace DigicoSD9.Service
{
    public class SD9ControlStateService
    {
        private readonly ConcurrentQueue<OscPacket> _incomingPacketsFromSD9;
        private bool _isStopping;
        private Logger _logger = LogManager.GetCurrentClassLogger();

        #region Constructors
        public SD9ControlStateService(IUnityContainer unityContainer)
        {
            _incomingPacketsFromSD9 = new ConcurrentQueue<OscPacket>();
            UnityContainer = unityContainer;
        }
        #endregion

        #region Properties
        public IUnityContainer UnityContainer { get; private set; }
        public ConcurrentQueue<OscPacket> IncomingPacketsFromSd9Queue
        {
            get { return _incomingPacketsFromSD9; }
        }

        public SD9Console ConsoleState
        {
            get { return UnityContainer.Resolve<SD9Console>(); }
        }

        #endregion

        #region Public Methods
        public void Start()
        {
            Task.Run((Action)ProcessIncomingPacketsFromSd9Queue);
        }

        public void Stop()
        {
            _isStopping = true;
            while (!_incomingPacketsFromSD9.IsEmpty)
            {
            }
        }  
        #endregion

        #region Private Methods
        private void ProcessIncomingPacketsFromSd9Queue()
        {
            _logger.Info("Incoming SD9 OSC Packet Queue Processor started");
            while (!_isStopping)
            {
                if (_incomingPacketsFromSD9.IsEmpty)
                {
                    Thread.Sleep(1);
                    continue;
                }

                OscPacket dequeuedPacket;
                while (_incomingPacketsFromSD9.TryDequeue(out dequeuedPacket))
                {
                    dequeuedPacket.ParseOscPacket(PacketParserCallback);
                }
            }
            _logger.Info("Incoming SD9 OSC Packet Queue Processor stoped");
        }
        #endregion

        #region Parsing Methods
        private void PacketParserCallback(string[] namespaces, bool isQuery, object[] arguments)
        {
            if (isQuery) //Ignore queries
                return;


            switch (namespaces[0])
            {
                case "Input_Channels":
                    ProcessInputChannels(namespaces, isQuery, arguments);
                    break;
                case "Console":
                    ProcessConsole(namespaces, isQuery, arguments);
                    break;
                case "Aux_Outputs":
                    ProcessAuxOutput(namespaces, isQuery, arguments);
                    break;
                case "Control_Groups":
                    ProcessControlGroups(namespaces, isQuery, arguments);
                    break;
                case "Group_Outputs":
                    ProcessGroupOutputs(namespaces, isQuery, arguments);
                    break;
                case "Matrix_Outputs":
                    ProcessMatrixOutputs(namespaces, isQuery, arguments);
                    break;
            }
        }

        private void ProcessMatrixOutputs(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte matrixOutNo = byte.Parse(namespaces[1]);
            var selMatrixOut = this.ConsoleState.MatrixOuts[matrixOutNo];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                    {
                        if (namespaces[3] == "name")
                        {
                            selMatrixOut.Name = arguments[0] as string;
                        }
                    }
                    break;
            }
        }

        private void ProcessGroupOutputs(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte groupNo = byte.Parse(namespaces[1]);
            var selectedGroupOut = this.ConsoleState.GroupOutputs[groupNo];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                    {
                        if (namespaces[3] == "name")
                        {
                            selectedGroupOut.Name = arguments[0] as string;
                        }
                    }
                    break;
            }
        }

        private void ProcessControlGroups(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte groupNo = byte.Parse(namespaces[1]);
            var selControlGroup = this.ConsoleState.ControlGroups[groupNo];
            switch (namespaces[2])
            {
                case "name":
                    {
                        selControlGroup.Name = arguments[0] as string;
                    }
                    break;
            }
        }

        private void ProcessAuxOutput(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte auxNum = byte.Parse(namespaces[1]);
            var selectedAuxOut = this.ConsoleState.AuxOutputs[auxNum];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                    {
                        if (namespaces[3] == "name")
                        {
                            selectedAuxOut.Name = arguments[0] as string;
                        }
                    }
                    break;
            }
        }

        private void ProcessConsole(string[] namespaces, bool isQuery, object[] arguments)
        {
            switch (namespaces[1])
            {
                case "Aux_Outputs":
                    if (namespaces[2] == "modes")
                    {
                        for (int i = 1; i <= arguments.Length; i++)
                        {
                            var lineMode = Convert.ToByte(arguments[i - 1]);
                            this.ConsoleState.AuxOutputs[i].LineMode = (LineMode)lineMode;
                        }
                    }
                    break;
                case "Input_Channels":
                    if (namespaces[2] == "modes")
                    {
                        for (int i = 1; i <= arguments.Length; i++)
                        {
                            var lineMode = Convert.ToByte(arguments[i - 1]);
                            this.ConsoleState.InputChannels[i].LineMode = (LineMode)lineMode;
                        }
                    }
                    break;
                case "Channels":
                    break;
                case "Name":
                    this.ConsoleState.Name = arguments[0] as string;
                    break;
            }
        }

        private void ProcessInputChannels(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte chNum = byte.Parse(namespaces[1]);
            var selectedChannel = this.ConsoleState.GetInputChannel(chNum);
            switch (namespaces[2])
            {
                case "Aux_Send":
                    {
                        var auxNum = byte.Parse(namespaces[3]);
                        var selAuxSend = selectedChannel.GetAuxSend(auxNum);

                        if (string.Equals(namespaces[4], "send_level", StringComparison.InvariantCultureIgnoreCase))
                        {
                            selAuxSend.Level = (float)arguments[0];
                            GlobalHost.ConnectionManager.GetHubContext<InputChannelHub, IInputChannelHub>().Clients.All.onAuxSendLevelMessage(chNum, auxNum, selAuxSend.Enabled, selAuxSend.Level);
                        }
                        else if (string.Equals(namespaces[4], "send_on", StringComparison.InvariantCultureIgnoreCase))
                        {
                            selAuxSend.Enabled = Math.Abs((float)arguments[0] - 1f) < 0.1f;
                            GlobalHost.ConnectionManager.GetHubContext<InputChannelHub, IInputChannelHub>().Clients.All.onAuxSendLevelMessage(chNum, auxNum, selAuxSend.Enabled, selAuxSend.Level);
                        }
                    }
                    break;
                case "Channel_Input":
                    {
                        if (namespaces[3] == "name")
                        {
                            selectedChannel.Name = arguments[0] as string;
                        }
                    }
                    break;
            }
        }
        #endregion
    }
}
