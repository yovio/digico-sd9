﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigicoSD9.Service.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using NLog;
using Topshelf;
using Topshelf.Unity;

namespace DigicoSD9.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                logger.Info("Load and run service host");
                var host = CreateServiceHost();
                host.Run();
            }
            catch (Exception ex)
            {
                logger.Error("Error starting service.", ex);
                throw;
            }
        }

        static IUnityContainer CreateUnityContainer()
        {
            var unityContainer = new UnityContainer();

            var configSection = ConfigurationManager.GetSection("unity") as UnityConfigurationSection;
            configSection.Configure(unityContainer);

            return unityContainer;
        }

        static Host CreateServiceHost()
        {
            var unityContainer = CreateUnityContainer();

            GlobalHost.DependencyResolver = new SignalRUnityDependencyResolver(unityContainer);
            return HostFactory.New(cfg =>
            {
                cfg.UseUnityContainer(unityContainer);


                cfg.Service<ServiceManager>(s =>
                {
                    s.ConstructUsingUnityContainer();
                    s.WhenStarted(proxy => proxy.Start());
                    s.WhenStopped(proxy => proxy.Stop());

                    //s.WhenShutdown(pub => pub.Shutdown());
                });

                cfg.EnableServiceRecovery(rc =>
                {
                    rc.RestartService(1);//restart service after 1 min
                    rc.SetResetPeriod(1);//reset interval is one day
                });

                cfg.StartAutomatically();
                //cfg.EnableShutdown();
                cfg.RunAsLocalService();
                cfg.SetDescription("Service to process message from and to SD9");
                cfg.SetDisplayName("Digico SD9 Proxy Service");
                cfg.SetServiceName("DigicoSD9Proxy");
                cfg.UseNLog();
            });
        }
    }
}
