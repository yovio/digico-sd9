using System.Collections.Generic;

using Nancy.BundleIt;

namespace DigicoSD9.Service
{
	/// <summary>
    /// Configures Nancy.BundleIt assets on application startup.
	/// There can only be 1 BundleItConfig.cs file per project. BundleIt will use the first one it finds.
    /// </summary>
    public class BundleItConfig : IBundleItConfig
    {
        private const string jqueryVer = "1.11.2";
        private const string jqueryUIVer = "1.11.4";
        private const string signalrVer = "2.2.0";

        public void Configure(Bundles bundles, ConfigSettings settings)
        {
            // Optional settings
            //settings.ScriptPath = "_scriptbundle";
            //settings.StylePath = "_stylebundle";
            //settings.ThrowExceptionWhenFileMissing = true;
            //settings.ForceDebugMode = false;
            //settings.ForceReleaseMode = true;

            bundles.AddScripts("~/js/jquery", new List<BundleItFile>
            {
                new BundleItFile(string.Format("/scripts/vendor/jquery/jquery-{0}.js", jqueryVer), string.Format("/scripts/vendor/jquery/jquery-{0}.min.js", jqueryVer))
            });

            bundles.AddScripts("~/js/jquery-ui", new List<BundleItFile>
            {
                new BundleItFile(string.Format("/scripts/vendor/jquery-ui/jquery-ui-{0}.js", jqueryUIVer), string.Format("/scripts/vendor/jquery-ui/jquery-ui-{0}.min.js", jqueryUIVer)),
                new BundleItFile("/scripts/vendor/jquery-ui/jquery-ui.toggleSwitch.js", "/scripts/vendor/jquery-ui/jquery-ui.toggleSwitch.min.js"),
                new BundleItFile("/scripts/vendor/jquery-ui/jquery.ui.touch-punch.js", "/scripts/vendor/jquery-ui/jquery.ui.touch-punch.min.js")
            });

            bundles.AddScripts("~/js/signalr", new List<BundleItFile>
            {
                new BundleItFile(string.Format("/scripts/vendor/signalr/jquery.signalR-{0}.js", signalrVer), string.Format("/scripts/vendor/signalr/jquery.signalR-{0}.min.js", signalrVer))
            });

            bundles.AddScripts("~/js/angular", new List<BundleItFile>
            {
                new BundleItFile("/scripts/vendor/angular/angular.js", "/scripts/vendor/angular/angular.min.js")
            });

            bundles.AddScripts("~/js/app", new List<BundleItFile>
            {
                new BundleItFile("/scripts/vendor/angular/angular-ui-router.js", "/scripts/vendor/angular/angular-ui-router.min.js"),
                new BundleItFile("/scripts/vendor/angular/angular-signalr-hub.js","/scripts/vendor/angular/angular-signalr-hub.min.js"),
                new BundleItFile("/scripts/filters.js"),
                new BundleItFile("/scripts/services.js"),
                new BundleItFile("/scripts/directives.js"),
                new BundleItFile("/scripts/controllers.js"),
                new BundleItFile("/scripts/app.js")
            });



            bundles.AddStyles("~/content/css/app", new List<BundleItFile>
            {
                new BundleItFile("/css/app.css")
            });

            bundles.AddStyles("~/content/css/jquery-ui", new List<BundleItFile>
            {
                new BundleItFile("/css/jquery-ui/themes/delta/jquery-ui.css")
            });
        }
    }
}
