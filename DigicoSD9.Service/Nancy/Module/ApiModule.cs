﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigicoSD9.Model;
using Microsoft.Practices.Unity;
using Nancy;
using Nancy.Responses;
using Newtonsoft.Json;
using Topshelf;
using Topshelf.Builders;

namespace DigicoSD9.Service.Nancy.Module
{
    public class ApiModule : NancyModule
    {
        ISerializer _jsonSerializer = null;

        public ApiModule() : base("/api")
        {
            Get["/AuxOutput/{id:int}"] = _ =>
            {
                var sd9Console = UnityContainer.Resolve<SD9Console>();

                if (_.id < 1 || _.id > sd9Console.AuxOutputs.Count)
                    return null;
                return Response.AsJson(new { sd9Console.AuxOutputs[_.id].Name }, HttpStatusCode.OK );
            };

            Get["/InputChannel/{id:int}"] = _ =>
            {
                var sd9Console = UnityContainer.Resolve<SD9Console>();

                if (_.id < 1 || _.id > sd9Console.InputChannels.Count)
                    return null;

                var inputCh = sd9Console.InputChannels[_.id];

                return Response.AsJson((object)inputCh);
            };

            Get["/AuxSend/{auxNo:int}"] = _ =>
            {
                var sd9Console = UnityContainer.Resolve<SD9Console>();

                byte auxNo = (byte)_.auxNo;
                if (auxNo < 1 || auxNo > sd9Console.AuxOutputs.Count)
                    return null;

                var result = new
                {
                    AuxNum = auxNo,
                    AuxName = sd9Console.AuxOutputs[auxNo].Name,
                    InputChannels = sd9Console.InputChannels.Select(inCh=> new
                    {
                        inCh.Value.Name,
                        ChNum = inCh.Key,
                        Value = inCh.Value.AuxSends[auxNo].Level,
                        inCh.Value.AuxSends[auxNo].Enabled
                    }).ToArray()
                };

                return Response.AsJson(result);
            };

            Get["/InputChannel/{chNo:int}/AuxSend/{auxNo:int}"] = _ =>
            {
                var sd9Console = UnityContainer.Resolve<SD9Console>();

                if (_.chNo < 1 || _.chNo > sd9Console.InputChannels.Count)
                    return null;

                InputChannel inputCh = sd9Console.InputChannels[_.chNo];

                if (_.auxNo < 1 || _.auxNo > inputCh.AuxSends.Count)
                    return null;

                return Response.AsJson((object)inputCh.AuxSends[_.auxNo]);
            };
        }

        [Dependency]
        public IUnityContainer UnityContainer { get; set; }
    }
}
