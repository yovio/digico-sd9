﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Nancy;

namespace DigicoSD9.Service.Nancy.Module
{
    public class IndexModule : NancyModule
    {
        [Dependency]
        public IUnityContainer UnityContainer { get; set; }

        public IndexModule()
        {
            Get["/"] = parameters =>
            {
                var viewName = "index";
                Context.Items["templateUrl"] = string.Format("/views/{0}", viewName);
                return View[viewName];
            };

            Get[@"/auxSend/{auxNo:int}"] = parameters =>
            {
                var viewName = "auxsend";
                Context.Items["templateUrl"] = string.Format("/views/{0}", viewName);
                return View[viewName];
            };

            Get[@"/{viewName}"] = parameters =>
            {
                var viewName = ((String)parameters.viewName);
                Context.Items["templateUrl"] = string.Format("/views/{0}", viewName);
                return View[viewName];
            };

            Get[@"/views/{viewName}"] = parameters =>
            {
                var viewName = ((String)parameters.viewName);
                Context.Items["templateUrl"] = string.Format("/views/{0}", viewName);
                return View[viewName];
            };
        }
    }
}
