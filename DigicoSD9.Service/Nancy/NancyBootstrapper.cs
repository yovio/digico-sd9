﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Unity;
using Nancy.Conventions;
using Nancy.Diagnostics;
using Nancy.ModelBinding;
using Nancy.TinyIoc;
using Nancy.ViewEngines;

namespace DigicoSD9.Service.Nancy
{
    public class NancyBootstrapper : UnityNancyBootstrapper
    {
        protected IUnityContainer _UnityContainer;
        public NancyBootstrapper(IUnityContainer unityContainer)
        {
            _UnityContainer = unityContainer;
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("css", "Content/css"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("scripts", "Content/Scripts"));
        }
        
        protected override void ConfigureApplicationContainer(IUnityContainer existingContainer)
        {
            // Perform registation that should have an application lifetime
            _UnityContainer.AddNewExtension<EnumerableExtension>();
        }

        protected override IUnityContainer GetApplicationContainer()
        {
            return _UnityContainer;
        }
    }
}
