﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace DigicoSD9.Service.SignalR.Hubs
{
    public class DigicoSD9Hub : Hub
    {
        public void Send(string address, string message)
        {
            Clients.All.addOscMessage(address, message);
        }
    }
}
