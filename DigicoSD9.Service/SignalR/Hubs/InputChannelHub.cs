﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigicoSD9.Model;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;
using Rug.Osc;

namespace DigicoSD9.Service.SignalR.Hubs
{
    public interface IInputChannelHub
    {
        void onAuxSendLevelMessage(byte channelNum, byte auxNum, bool enable, float value);
    }

    public class InputChannelHub : Hub<IInputChannelHub>
    {
        private SD9ProxyService _sd9ProxyService = GlobalHost.DependencyResolver.Resolve<SD9ProxyService>();
        private SD9Console _sd9Console = GlobalHost.DependencyResolver.Resolve<SD9Console>();

        public void GetAuxSendLevel(byte channelNum, byte auxNum)
        {
            var oscMessage = new OscMessage(string.Format("/Input_Channels/{0}/Aux_Send/{1}/send_level/?", channelNum, auxNum));
            _sd9ProxyService.OutgoingMsgQueue.Enqueue(oscMessage);
            //Clients.All.onAuxSendLevelMessage(channelNum, auxNum, (float)-13 * channelNum);
        }

        public void SetAuxSendLevel(byte channelNum, byte auxNum, float value)
        {
            var auxSend = _sd9Console.InputChannels[channelNum].AuxSends[auxNum];

            var oscMessage = new OscMessage(string.Format("/Input_Channels/{0}/Aux_Send/{1}/send_level", channelNum, auxNum), value);
            auxSend.Level = value;
            _sd9ProxyService.OutgoingMsgQueue.Enqueue(oscMessage);

            //Broadcast to other client
            Clients.Others.onAuxSendLevelMessage(channelNum, auxNum, auxSend.Enabled, value);
        }
    }
}