﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;

namespace DigicoSD9.Service.SignalR
{
    public class SignalRUnityDependencyResolver : DefaultDependencyResolver
    {
        private IUnityContainer _container;

        public SignalRUnityDependencyResolver(IUnityContainer container)
        {
            _container = container;
        }

        public IUnityContainer Container
        {
            get { return _container; }
        }

        public override object GetService(Type serviceType)
        {
            if (Container.IsRegistered(serviceType)) return Container.Resolve(serviceType);
            else return base.GetService(serviceType);
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            if (Container.IsRegistered(serviceType)) return Container.ResolveAll(serviceType);
            else return base.GetServices(serviceType);
        }

    }
}
