﻿'use strict';

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('app.services', [])
    .value('version', '0.1')
    .factory('inputChHub', ['Hub', function (Hub) {

    var dummyListener = function() {
    };

        var inputChHub = new Hub('inputChannelHub', {
            logging: true,
            transport: ['webSockets', 'longPolling'],
            rootPath: "/signalr",
            methods: ['getAuxSendLevel', 'setAuxSendLevel'],
            listeners: {
                'onDummy': dummyListener
            },
            //stateChanged: onHubStateChange,
            errorHandler: function (error) {
                console.error(error);
            }
        });
    
    return inputChHub;
}]);