﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers', [])

    // Path: /
    .controller('HomeCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA Template';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /about
    .controller('AboutCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | About';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /login
    .controller('LoginCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | Sign In';
        // TODO: Authorize a user
        $scope.login = function () {
            $location.path('/');
            return false;
        };
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /about
    .controller('AuxSendCtrl', ['$http', '$scope', '$location', '$window', 'inputChHub', function ($http, $scope, $location, $window, inputChHub) {
        $scope.$root.title = 'Digico SD9 | Aux Send from scope';
        $scope.$on('$viewContentLoaded', function() {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        var auxNum = $location.path().split("/")[2] || 1;

        $http.get('/api/auxsend/' + auxNum).
            success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
            $scope.AuxSend = data;
        });

        var onAuxSendLevelMessage = function (channelNum, auxNum, enable, value) {
            if (auxNum != $scope.AuxSend.AuxNum)
                return;

            for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
                if ($scope.AuxSend.InputChannels[i].ChNum == channelNum) {
                    $scope.$apply(function () {
                        $scope.AuxSend.InputChannels[i].Enabled = enable;
                        $scope.AuxSend.InputChannels[i].lastSyncValue = value;
                        $scope.AuxSend.InputChannels[i].Value = value;
                    });
                }
            }
        };

        inputChHub.on('onAuxSendLevelMessage', onAuxSendLevelMessage);

        inputChHub.promise.done(function() {
            //for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
            //    var inCh = $scope.AuxSend.InputChannels[i];

            //    if (!inCh.Enabled)
            //        continue;
            //    inputChHub.getAuxSendLevel(inCh.ChNum, $scope.AuxSend.AuxNum);
            //}
        });
        
        //Send slider value every 100ms if changes
        var sendValue = function (auxNum, inputCh) {
            if (inputCh.Value && (!inputCh.lastSyncValue || Math.abs(inputCh.Value - inputCh.lastSyncValue) >= 0.1)) {
                inputCh.lastSyncValue = inputCh.Value;
                inputChHub.setAuxSendLevel(inputCh.ChNum, auxNum, inputCh.Value);
            }
        };

        setInterval(function () {
            if (inputChHub.connection.state == 1) {
                for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
                    sendValue($scope.AuxSend.AuxNum, $scope.AuxSend.InputChannels[i]);
                }
            }
            
        }, 50);

    }])

    // Path: /error/404
    .controller('Error404Ctrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'Error 404: Page Not Found';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);