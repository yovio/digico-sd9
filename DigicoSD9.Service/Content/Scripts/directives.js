﻿'use strict';

angular.module('app.directives', [])
    .directive('appVersion', [
        'version', function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }
    ])

    .directive('auxSendControl', function () {
        return {
            restrict: 'E',
            template: "<div style=\"display: table\"><span style=\"width:100%;height:18px;display:table-caption;\" class=\"label label-info\">{{auxSend.Value | number:2}}dB</span>" +
                      "<button style=\"display:table-row;vertical-align: top;margin-right:20px;margin-left:20px;margin-top:5px;background-position: 0 0px;\" type=\"button\" class=\"btn btn-default\" aria-label=\"Increase\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span></button>" +
                      "<div style=\"height:130px;margin-left:auto;margin-right:auto;margin-top:15px;margin-bottom:10px;text-align:center;vertical-align: middle;\"></div>" +
                      "<button style=\"display:table-row;vertical-align: bottom;margin-right:20px;margin-left:20px;margin-top:1px;background-position: 0 0px;\" type=\"button\" class=\"btn btn-default\" aria-label=\"Decrease\"><span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span></button>" +
                      "<span style=\"width:98%;display:inline-block;vertical-align: bottom;\" class=\"label label-primary\">{{auxSend.Name}}</span></div>",
            scope: {
                option: "=option",
                auxSend: "="
            },
            replace: true,
            link: function (scope, elm, attrs) {
                var container = elm;
                var lblValue = container.children().first();

                var increaseBtn = lblValue.next();
                var sliderDiv = increaseBtn.next();
                var decreaseBtn = sliderDiv.next();
                
                sliderDiv.slider(scope.option);

                sliderDiv.on("slide", function (event, ui) {
                    scope.$apply(function () {
                        scope.auxSend.Value = ui.value;
                    });
                });

                var incDecAction;
                increaseBtn.on('mousedown touchstart', function () {
                    if (decreaseBtn.prop("disabled", true))
                        decreaseBtn.prop("disabled", false);

                    $(this).removeClass("mobileHoverFix");
                    incDecAction = setInterval(function () {
                        if (sliderDiv.slider("option", "max") == undefined || scope.auxSend.Value < sliderDiv.slider("option", "max")) {
                            scope.$apply(function () {
                                scope.auxSend.Value += 0.20;
                            });
                        } else {
                            increaseBtn.prop("disabled", true);
                            clearInterval(incDecAction);
                        }
                    }, 50);
                }).on('mouseup touchend', function () {
                    $(this).addClass("mobileHoverFix");
                    clearInterval(incDecAction);
                });

                decreaseBtn.on('mousedown touchstart', function () {
                    $(this).removeClass("mobileHoverFix");
                    incDecAction = setInterval(function () {
                        if (increaseBtn.prop("disabled", true))
                            increaseBtn.prop("disabled", false);

                        if (sliderDiv.slider("option", "min") == undefined || scope.auxSend.Value > sliderDiv.slider("option", "min")) {
                            scope.$apply(function () {
                                scope.auxSend.Value -= 0.20;
                            });
                        } else {
                            decreaseBtn.prop("disabled", true);
                            clearInterval(incDecAction);
                        }
                    }, 50);
                }).on('mouseup touchend', function () {
                    $(this).addClass("mobileHoverFix");
                    clearInterval(incDecAction);
                });

                scope.$watch(
                    function (scope) { return scope.auxSend.Value },
                    function (newValue) {
                        sliderDiv.slider("value", newValue);
                        //lblValue.text(newValue + " dB");
                    });
            }
    };
});