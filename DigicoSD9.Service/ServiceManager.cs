﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Service
{
    public class ServiceManager
    {
        private readonly SD9ProxyService _sd9ProxyService;
        private readonly WebServices _webServices;

        public ServiceManager(SD9ProxyService sd9ProxyService, WebServices webServices)
        {
            this._sd9ProxyService = sd9ProxyService;
            this._webServices = webServices;
        }

        public void Start()
        {            
            _sd9ProxyService.Start();
            _webServices.Start();
        }

        public void Stop()
        {
            _webServices.Stop();
            _sd9ProxyService.Stop();
        }
    }
}
