﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DigicoSD9.Model;
using DigicoSD9.Utility;
using Microsoft.Practices.Unity;
using NLog;
using Rug.Osc;

namespace DigicoSD9.Service
{
    public class SD9ProxyService
    {
        private const byte MESAGE_FROM_SD9 = 0;
        private const byte MESAGE_FROM_IPAD = 1;

        private bool _isStopping;
        private bool _isSD9ListenerStarted;
        private bool _isIpadListenerStarted;
        private readonly ConcurrentQueue<Tuple<DateTime, byte, IPEndPoint, byte[]>> _incomingMsgQueue = new ConcurrentQueue<Tuple<DateTime, byte, IPEndPoint, byte[]>>();
        private readonly ConcurrentQueue<OscMessage> _outgoingMsgQueue = new ConcurrentQueue<OscMessage>();
        private CancellationTokenSource _serviceStopTokenSource = new CancellationTokenSource();
        private IPEndPoint _sd9ReceiveEndPoint;
        private int _sd9SendPort;

        private IPEndPoint _ipadReceiveEndPoint;
        Logger _logger = LogManager.GetCurrentClassLogger();
        

        #region Public Methods
        public void Start()
        {
            Thread.Sleep(1000); //Wait for SD9 Simulator ready
            var sd9Ip = IPAddress.Parse(ConfigurationManager.AppSettings["SD9IP"]);
            _sd9SendPort = int.Parse(ConfigurationManager.AppSettings["SD9SendPort"]);
            var sd9RcvPort = int.Parse(ConfigurationManager.AppSettings["SD9RcvPort"]);

            var ipadIp = IPAddress.Parse(ConfigurationManager.AppSettings["IpadIP"]);
            var ipadSendPort = int.Parse(ConfigurationManager.AppSettings["IpadSendPort"]);
            var ipadRcvPort = int.Parse(ConfigurationManager.AppSettings["IpadRcvPort"]);

            _sd9ReceiveEndPoint = new IPEndPoint(sd9Ip, sd9RcvPort);
            _ipadReceiveEndPoint = new IPEndPoint(ipadIp, ipadRcvPort);

            if (GetConsoleStructure())
            {
                UnityContainer.Resolve<SD9ControlStateService>().Start();
                UdpListener(_sd9SendPort, ipadSendPort);
                Task.Run((Action) ProcessQueue);
                Task.Run((Action) ProcessOutgoingQueue);

                Thread.Sleep(50);
                var sd9Console = UnityContainer.Resolve<SD9Console>();

                //Ask for Input channel & Aux Outputs line modes
                _outgoingMsgQueue.Enqueue(new OscMessage("/Console/Input_Channels/modes/?"));
                _outgoingMsgQueue.Enqueue(new OscMessage("/Console/Aux_Outputs/modes/?"));

                //Ask for Aux Out names
                foreach (var auxAutKvp in sd9Console.AuxOutputs)
                {
                    _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Aux_Outputs/{0}/Buss_Trim/name/?", auxAutKvp.Key)));
                }

                //Ask for Channel Names & Aux Send
                foreach (var inChannelKvp in sd9Console.InputChannels)
                {
                    _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Input_Channels/{0}/Channel_Input/name/?", inChannelKvp.Key)));
                    foreach (var auxAutKvp in sd9Console.AuxOutputs)
                    {
                        _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Input_Channels/{0}/Aux_Send/{1}/send_level/?", inChannelKvp.Key, auxAutKvp.Key)));
                        _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Input_Channels/{0}/Aux_Send/{1}/send_on/?", inChannelKvp.Key, auxAutKvp.Key)));
                    }
                }

                //Ask for CG
                foreach (var cgKvp in sd9Console.ControlGroups)
                {
                    _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Control_Groups/{0}/name/?", cgKvp.Key)));
                }

                //Ask for Group_Output
                foreach (var goKvp in sd9Console.GroupOutputs)
                {
                    _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Group_Outputs/{0}/Buss_Trim/name/?", goKvp.Key)));
                }

                //Ask for Matrix_Output
                foreach (var moKvp in sd9Console.MatrixOuts)
                {
                    _outgoingMsgQueue.Enqueue(new OscMessage(string.Format("/Matrix_Outputs/{0}/Buss_Trim/name/?", moKvp.Key)));
                }
            }
        }

        public void Stop()
        {
            _isStopping = true;
            _serviceStopTokenSource.Cancel(true);
            while (_isIpadListenerStarted || _isSD9ListenerStarted || !_incomingMsgQueue.IsEmpty || !OutgoingMsgQueue.IsEmpty)
            {
            }
            UnityContainer.Resolve<SD9ControlStateService>().Stop();
        } 
        #endregion

        #region Properties
        [Dependency]
        public IUnityContainer UnityContainer { get; set; }

        public ConcurrentQueue<OscMessage> OutgoingMsgQueue
        {
            get { return _outgoingMsgQueue; }
        }
        #endregion

        #region Private Methods

        private bool GetConsoleStructure()
        {
            var consoleNameQueryResponse = SendAndWaitForResponse(_sd9ReceiveEndPoint, _sd9SendPort, 10000, new OscMessage("/Console/Name/?"));

            if (object.Equals(consoleNameQueryResponse, null) || consoleNameQueryResponse.Count == 0 || consoleNameQueryResponse[0].Address != "/Console/Name" || consoleNameQueryResponse[0][0] as string != "SD9")
            {
                _logger.Error("Digico SD9 console is not responding or its not a Digico SD9");
                return false;
            }

            var sd9Console = new SD9Console();
            var expNoOfAnswer = 9;
            var consoleChQueryResponses = SendAndWaitForResponse(_sd9ReceiveEndPoint, _sd9SendPort, 2000, new OscMessage("/Console/Channels/?"), expNoOfAnswer);
            if (object.Equals(consoleChQueryResponses, null) || consoleChQueryResponses.Count != 9)
            {
                _logger.Error("Digico SD9 did not replying with proper channels informations.");
            }
            else
            {
                var noOfInputCh = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Input_Channels"))[0]);
                var noOfAuxOut = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Aux_Outputs"))[0]);
                var noOfGroupOut = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Group_Outputs"))[0]);
                var noOfTalkbackOut = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Talkback_Outputs"))[0]);
                var noOfCG = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Control_Groups"))[0]);
                var noOfMatrixIn = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Matrix_Inputs"))[0]);
                var noOfMatrixout = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Matrix_Outputs"))[0]);
                var noOfGEQ = Convert.ToByte(consoleChQueryResponses.First(msg => string.Equals(msg.Address, "/Console/Graphic_EQ"))[0]);

                sd9Console.SetConsoleStructure(noOfAuxOut, noOfInputCh, noOfGroupOut, noOfCG, noOfMatrixIn,
                    noOfMatrixout, noOfGEQ, noOfTalkbackOut);
            }

            UnityContainer.RegisterInstance(sd9Console);

            return true;
        }

        private void UdpListener(int sd9SendPort = 8000, int ipadSendPort = 9001)
        {
            #region Task Listening send message from SD9
            Task.Run(async () =>
            {
                using (var udpSender = new UdpClient())
                {
                    using (var udpListener = new UdpClient(sd9SendPort))
                    {
                        var logger = LogManager.GetCurrentClassLogger();
                        logger.Info("UDP SD9 Listener started and listening message coming on port {0}", sd9SendPort);

                        _isSD9ListenerStarted = true;
                        while (!_isStopping)
                        {
                            try
                            {
                                var receivedResults = await udpListener.ReceiveAsync().WithCancellation(_serviceStopTokenSource.Token);
                                udpSender.Send(receivedResults.Buffer, receivedResults.Buffer.Length, _ipadReceiveEndPoint);
                                _incomingMsgQueue.Enqueue(new Tuple<DateTime, byte, IPEndPoint, byte[]>(DateTime.Now, MESAGE_FROM_SD9, receivedResults.RemoteEndPoint, receivedResults.Buffer));
                            }
                            catch (OperationCanceledException ce)
                            {
                            }
                            catch (Exception ex)
                            {
                                logger.ErrorException("Exception happen when listening UDP from SD9", ex);
                            }
                        }
                        _isSD9ListenerStarted = false;
                        logger.Info("UDP SD9 Listener stoped");
                    }
                }
            });
            #endregion

            #region Task Listening send message from Ipad
            Task.Run(async () =>
            {
                using (var udpSender = new UdpClient())
                {
                    #region Send IPAD Magic packet to SD9
                    var magicPacketBytes = Convert.FromBase64String(@"L0NvbnNvbGUvTmFtZS8/");
                    udpSender.Send(magicPacketBytes, magicPacketBytes.Length, _sd9ReceiveEndPoint); 
                    #endregion

                    using (var udpListener = new UdpClient(ipadSendPort))
                    {
                        var logger = LogManager.GetCurrentClassLogger();
                        logger.Info("UDP IPAD Listener started and listening message coming in port {0}", ipadSendPort);
                        _isIpadListenerStarted = true;
                        while (!_isStopping)
                        {
                            try
                            {
                                var receivedResults =
                                    await udpListener.ReceiveAsync().WithCancellation(_serviceStopTokenSource.Token);
                                udpSender.Send(receivedResults.Buffer, receivedResults.Buffer.Length, _sd9ReceiveEndPoint);
                                _incomingMsgQueue.Enqueue(new Tuple<DateTime, byte, IPEndPoint, byte[]>(DateTime.Now,
                                    MESAGE_FROM_IPAD, receivedResults.RemoteEndPoint, receivedResults.Buffer));
                            }
                            catch (OperationCanceledException ce)
                            {
                            }
                            catch (Exception ex)
                            {
                                logger.ErrorException("Exception happen when listening UDP from IPAD", ex);
                            }
                        }
                        _isIpadListenerStarted = false;
                        logger.Info("UDP IPAD Listener stoped");
                    }
                }
            });
            #endregion
        }

        private void ProcessOutgoingQueue()
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("Outgoing Message Queue Processor started");

            using (UdpClient sender = new UdpClient())
            {
                sender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                sender.Connect(_sd9ReceiveEndPoint);
                while (!_isStopping)
                {
                    if (OutgoingMsgQueue.IsEmpty)
                    {
                        Thread.Sleep(1);
                        continue;
                    }

                    OscMessage dequeuedOsc;
                    while (OutgoingMsgQueue.TryDequeue(out dequeuedOsc))
                    {
                        try
                        {
                            OscMessage peekedOscMessage;
                            if (OutgoingMsgQueue.TryPeek(out peekedOscMessage))
                            {
                                if (string.Equals(peekedOscMessage.Address, dequeuedOsc.Address))
                                //Next message is having same address, so skip and send the next instead
                                {
                                    logger.Debug("Skip Send OSC Message {0} to {1}:{2}", dequeuedOsc, _sd9ReceiveEndPoint.Address, _sd9ReceiveEndPoint.Port);
                                    continue;
                                }
                            }

                            var bytes = dequeuedOsc.ToByteArray();
                            sender.Send(bytes, bytes.Length);
                            logger.Debug("Send OSC Message {0} to {1}:{2}", dequeuedOsc, _sd9ReceiveEndPoint.Address, _sd9ReceiveEndPoint.Port);
                            Thread.Sleep(5);
                        }
                        catch (Exception)
                        {
                            logger.Warn("Failed sending OSC Message to SD9: {0}", dequeuedOsc);
                        }
                    }
                }
                sender.Close();
                logger.Info("Outgoing Message Queue Processor stoped");
            }

            #region Sender Using OSCSender
            //using (OscSender oscSender = new OscSender(_sd9ReceiveEndPoint.Address, _sd9ReceiveEndPoint.Port))
            //{
            //    oscSender.Connect();
            //    while (!_isStopping)
            //    {
            //        if (OutgoingMsgQueue.IsEmpty)
            //        {
            //            Thread.Sleep(1);
            //            continue;
            //        }

            //        OscMessage dequeuedOsc;
            //        while (OutgoingMsgQueue.TryDequeue(out dequeuedOsc))
            //        {
            //            try
            //            {
            //                oscSender.Send(dequeuedOsc);
            //                logger.Debug("Send OSC Message {0} to {1}:{2}", dequeuedOsc, _sd9ReceiveEndPoint.Address, _sd9ReceiveEndPoint.Port);
            //            }
            //            catch (Exception)
            //            {
            //                logger.Warn("Failed sending OSC Message to SD9: {0}", dequeuedOsc);
            //            }
            //        }
            //    }
            //    oscSender.Close();
            //    logger.Info("Outgoing Message Queue Processor stoped");
            //} 
            #endregion
        }

        private void ProcessQueue()
        {
            var logger = LogManager.GetCurrentClassLogger();
            var sd9ControlStateSvc = UnityContainer.Resolve<SD9ControlStateService>();
            logger.Info("UDP Queue Processor started");
            while (!_isStopping)
            {
                if (_incomingMsgQueue.IsEmpty)
                {
                    Thread.Sleep(1);
                    continue;
                }

                Tuple<DateTime, byte, IPEndPoint, byte[]> dequeuedMessage;
                while (_incomingMsgQueue.TryDequeue(out dequeuedMessage))
                {
                    var msgOrigin = dequeuedMessage.Item2 == MESAGE_FROM_SD9 ? "SD9" : "IPAD";
                    try
                    {
                        var oscPacket = OscPacket.Read(dequeuedMessage.Item4, dequeuedMessage.Item4.Length, dequeuedMessage.Item3);
                        if (dequeuedMessage.Item2 == MESAGE_FROM_SD9)
                        {
                            sd9ControlStateSvc.IncomingPacketsFromSd9Queue.Enqueue(oscPacket);
                        }
                        else if (dequeuedMessage.Item2 == MESAGE_FROM_IPAD)
                        {
                        }
                        logger.Debug("{0}: UDP Message as OSC : {1}\r\n", msgOrigin, oscPacket);
                    }
                    catch (Exception)
                    {
                        logger.Warn("{0}:  Unable to parse UDP Message as OSC. Original Message in Base64 : {1}\r\n", msgOrigin, Convert.ToBase64String(dequeuedMessage.Item4));
                    }
                }
            }
            logger.Info("UDP Queue Processor stoped");
        }

        private List<OscMessage> SendAndWaitForResponse(IPEndPoint destEP, int receivePort, int timeout, OscMessage messageToSend, int expectedNoOfResponse = 1)
        {
            using (var udpListener = new UdpClient(receivePort))
            {
                var remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                udpListener.Client.ReceiveTimeout = timeout;

                using (UdpClient sender = new UdpClient())
                {
                    sender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                    try
                    {
                        sender.Connect(destEP);

                        var messageToSendBytes = messageToSend.ToByteArray();
                        sender.Send(messageToSendBytes, messageToSendBytes.Length);

                        List<OscMessage> responses = new List<OscMessage>();
                        for (int i = 0; i < expectedNoOfResponse; i++)
                        {
                            var receiveBytes = udpListener.Receive(ref remoteIpEndPoint);
                            var responseMessage = OscMessage.Read(receiveBytes, receiveBytes.Length);
                            responses.Add(responseMessage);
                        }
                        return responses;
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            return null;

        }
        #endregion
    }
}
