﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class AuxOutput : BaseModel
    {
        private string _name;
        private LineMode _lineMode;

        #region Properties
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public LineMode LineMode
        {
            get { return _lineMode; }
            set { SetProperty(ref _lineMode, value); }
        }
        #endregion
    }
}
