﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class AuxSend : BaseModel
    {
        private float _level = -100;
        private bool _enabled;

        public float Level
        {
            get { return _level; }
            set { SetProperty(ref _level, value); }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { SetProperty(ref _enabled , value); }
        }
    }
}
