﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public enum BankFaderType
    {
        Empty,
        Aux_Outputs,
        Control_Groups,
        Group_Outputs,
        Input_Channels,
        Matrix_Outputs
    }

    public class BankFader : BaseModel
    {
        private BankFaderType _bankFaderType;
        private byte _channelIndex;

        public BankFaderType BankFaderType
        {
            get { return _bankFaderType; }
            set { SetProperty(ref _bankFaderType, value); }
        }

        public byte ChannelIndex
        {
            get { return _channelIndex; }
            set { SetProperty(ref _channelIndex, value); }
        }
    }
}
