﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    /// <summary>
    ///     This class adds the ability to refresh the list when any property of
    ///     the objects changes in the list which implements the INotifyPropertyChanged. 
    /// </summary>
    /// <typeparam name="TValue">
    public class ItemsChangeObservableDictionary<TKey,TValue> : ObservableDictionary<TKey,TValue>, IDisposable where TValue : BaseModel
    {
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var newValues = e.NewItems.OfType<KeyValuePair<TKey, TValue>>().Select(kvp => kvp.Value).ToArray();
                RegisterPropertyChanged(newValues);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                var oldValues = e.OldItems.OfType<KeyValuePair<TKey, TValue>>().Select(kvp => kvp.Value).ToArray();
                UnRegisterPropertyChanged(oldValues);
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                var newValues = e.NewItems.OfType<KeyValuePair<TKey, TValue>>().Select(kvp => kvp.Value).ToArray();
                var oldValues = e.OldItems.OfType<KeyValuePair<TKey, TValue>>().Select(kvp => kvp.Value).ToArray();

                UnRegisterPropertyChanged(oldValues);
                RegisterPropertyChanged(newValues);
            }
            
            base.OnCollectionChanged(e);
        }

        protected override void ClearItems()
        {
            UnRegisterPropertyChanged(base.Values.ToArray());
            base.ClearItems();
        }

        private void RegisterPropertyChanged(IList items)
        {
            foreach (INotifyPropertyChanged item in items)
            {
                if (item != null)
                {
                    item.PropertyChanged += new PropertyChangedEventHandler(item_PropertyChanged);
                }
            }
        }

        private void UnRegisterPropertyChanged(IList items)
        {
            foreach (INotifyPropertyChanged item in items)
            {
                if (item != null)
                {
                    item.PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
                }
            }
        }

        private void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            base.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #region IDisposable
		   // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~ItemsChangeObservableDictionary()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Clear(); //Make sure call Clear to remove any event subscription to prevent Memory Leaks
            }
            // free native resources here if there are any
        } 
	    #endregion
    }
}

