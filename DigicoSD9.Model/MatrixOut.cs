﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class MatrixOut : BaseModel
    {
        private string _name;
        private float _fader;
        private bool _mute;

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public float Fader
        {
            get { return _fader; }
            set { SetProperty(ref _fader, value); }
        }

        public bool Mute
        {
            get { return _mute; }
            set { SetProperty(ref _mute, value); }
        }
    }
}
