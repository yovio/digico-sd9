﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public enum LineMode : byte
    {
        Mono = 1,
        Stereo = 2
    }
}
