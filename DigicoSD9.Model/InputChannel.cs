﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class InputChannel : BaseModel, IDisposable
    {
        private float _fader;
        private string _name;
        private LineMode _lineMode;

        public InputChannel(byte noOfAux)
        {
            AuxSends = new ItemsChangeObservableDictionary<int, AuxSend>();
            MakeSureAuxSendAvailable(noOfAux);
            LineMode = LineMode.Mono;
        }

        #region Properties
        public ItemsChangeObservableDictionary<int, AuxSend> AuxSends { get; private set; }

        public float Fader
        {
            get { return _fader; }
            set { SetProperty(ref _fader, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public LineMode LineMode
        {
            get { return _lineMode; }
            set { SetProperty(ref _lineMode, value); }
        }
        #endregion

        #region Public Methods
        public AuxSend GetAuxSend(byte auxNum)
        {
            return AuxSends[auxNum];
        }
        #endregion

        #region Private Methods
        private void MakeSureAuxSendAvailable(byte auxNumber)
        {
            if (this.AuxSends.Count < auxNumber)
            {
                for (int i = AuxSends.Count + 1; i <= auxNumber; i++)
                {
                    AuxSends.Add(i, new AuxSend());
                }
            }
        }
        #endregion

        #region IDisposable
        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~InputChannel()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(AuxSends != null)
                    AuxSends.Dispose();
            }
            // free native resources here if there are any
        } 
	    #endregion
    }
}
