﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class SD9Console : BaseModel, IDisposable
    {
        private string _name;

        public SD9Console()
        {
            AuxOutputs = new ItemsChangeObservableDictionary<int, AuxOutput>();
            ControlGroups = new ItemsChangeObservableDictionary<int, ControlGroup>();
            GraphicEQs = new ItemsChangeObservableDictionary<int, GraphicEQ>();
            GroupOutputs = new ItemsChangeObservableDictionary<int, GroupOutput>();
            InputChannels = new ItemsChangeObservableDictionary<int, InputChannel>();
            MatrixIns = new ItemsChangeObservableDictionary<int, MatrixIn>();
            MatrixOuts = new ItemsChangeObservableDictionary<int, MatrixOut>();
            TalkbackOuts = new ItemsChangeObservableDictionary<int, TalkbackOut>();

            Banks = new ItemsChangeObservableDictionary<string, Bank>();
        }

        #region Properties
        public ItemsChangeObservableDictionary<int, AuxOutput> AuxOutputs { get; private set; }
        public ItemsChangeObservableDictionary<int, ControlGroup> ControlGroups { get; private set; }
        public ItemsChangeObservableDictionary<int, GraphicEQ> GraphicEQs { get; private set; }
        public ItemsChangeObservableDictionary<int, GroupOutput> GroupOutputs { get; private set; }
        public ItemsChangeObservableDictionary<int, InputChannel> InputChannels { get; private set; }
        public ItemsChangeObservableDictionary<int, MatrixIn> MatrixIns { get; private set; }
        public ItemsChangeObservableDictionary<int, MatrixOut> MatrixOuts { get; private set; }
        public ItemsChangeObservableDictionary<int, TalkbackOut> TalkbackOuts { get; private set; }

        public ItemsChangeObservableDictionary<string, Bank> Banks { get; private set; }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        #endregion

        #region Public Methods
        public void SetConsoleStructure(byte auxOutputNumber, byte chNumber, byte groupOutputNumber, byte noOfCG,
            byte matrixInNumber, byte matrixOutNumber, byte eqNumber, byte talkbackOutNo)
        {
            MakeSureAuxOutputAvailable(auxOutputNumber);
            MakeSureChannelAvailable(chNumber, auxOutputNumber);
            MakeSureControlGroupAvailable(noOfCG);
            MakeSureGraphicEqAvailable(eqNumber);
            MakeSureGroupOutputAvailable(groupOutputNumber);
            MakeSureMatrixInAvailable(matrixInNumber);
            MakeSureMatrixOutAvailable(matrixOutNumber);
            MakeSureTalkbackOutAvailable(talkbackOutNo);
        }

        public InputChannel GetInputChannel(byte channelNumber)
        {
            return InputChannels[channelNumber];
        }
        #endregion

        #region Private Methods
        private void MakeSureAuxOutputAvailable(byte ouxOutputNumber)
        {
            if (this.AuxOutputs.Count < ouxOutputNumber)
            {
                for (int i = AuxOutputs.Count + 1; i <= ouxOutputNumber; i++)
                {
                    AuxOutputs.Add(i, new AuxOutput());
                }
            }
        }

        private void MakeSureControlGroupAvailable(byte controlGroupNumber)
        {
            if (this.ControlGroups.Count < controlGroupNumber)
            {
                for (int i = ControlGroups.Count + 1; i <= controlGroupNumber; i++)
                {
                    ControlGroups.Add(i, new ControlGroup());
                }
            }
        }

        private void MakeSureGraphicEqAvailable(byte graphicEqNumber)
        {
            if (this.GraphicEQs.Count < graphicEqNumber)
            {
                for (int i = GraphicEQs.Count + 1; i <= graphicEqNumber; i++)
                {
                    GraphicEQs.Add(i, new GraphicEQ());
                }
            }
        }

        private void MakeSureGroupOutputAvailable(byte groupOutputNumber)
        {
            if (this.GroupOutputs.Count < groupOutputNumber)
            {
                for (int i = GroupOutputs.Count + 1; i <= groupOutputNumber; i++)
                {
                    GroupOutputs.Add(i, new GroupOutput());
                }
            }
        }

        private void MakeSureChannelAvailable(byte chNumber, byte noOfAux)
        {
            if (this.InputChannels.Count < chNumber)
            {
                for (int i = InputChannels.Count + 1; i <= chNumber; i++)
                {
                    InputChannels.Add(i, new InputChannel(noOfAux));
                }
            }
        }

        private void MakeSureMatrixInAvailable(byte matrixInNumber)
        {
            if (this.MatrixIns.Count < matrixInNumber)
            {
                for (int i = MatrixIns.Count + 1; i <= matrixInNumber; i++)
                {
                    MatrixIns.Add(i, new MatrixIn());
                }
            }
        }

        private void MakeSureMatrixOutAvailable(byte matrixOutNumber)
        {
            if (this.MatrixOuts.Count < matrixOutNumber)
            {
                for (int i = MatrixOuts.Count + 1; i <= matrixOutNumber; i++)
                {
                    MatrixOuts.Add(i, new MatrixOut());
                }
            }
        }

        private void MakeSureTalkbackOutAvailable(byte talkbackOutNo)
        {
            if (this.TalkbackOuts.Count < talkbackOutNo)
            {
                for (int i = TalkbackOuts.Count + 1; i <= talkbackOutNo; i++)
                {
                    TalkbackOuts.Add(i, new TalkbackOut());
                }
            }
        }
        #endregion

        #region IDisposable
        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~SD9Console()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (AuxOutputs != null)
                    AuxOutputs.Dispose();
                if (ControlGroups != null)
                    ControlGroups.Dispose();
                if (GraphicEQs != null)
                    GraphicEQs.Dispose();
                if (GroupOutputs != null)
                    GroupOutputs.Dispose();
                if (InputChannels != null)
                    InputChannels.Dispose();
                if (MatrixIns != null)
                    MatrixIns.Dispose();
                if (MatrixOuts != null)
                    MatrixOuts.Dispose();
                if (TalkbackOuts != null)
                    TalkbackOuts.Dispose();

                if (Banks != null)
                    Banks.Dispose();
            }
            // free native resources here if there are any
        } 
	    #endregion
    }
}
