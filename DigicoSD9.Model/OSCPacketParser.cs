﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rug.Osc;

namespace DigicoSD9.Model
{
    public static class OSCPacketParser
    {
        public static void ParseOscPacket(this OscPacket oscPacket, Action<string[], bool, object[]> callback)
        {
            if (oscPacket is OscBundle)
            {
                var oscBundle = oscPacket as OscBundle;
                foreach (var oscPacketinBundle in oscBundle)
                {
                    oscPacketinBundle.ParseOscPacket(callback);
                }
            }
            else if (oscPacket is OscMessage)
            {
                var oscMessage = oscPacket as OscMessage;
                ParseOscMessage(oscMessage, callback);
            }
        }

        private static void ParseOscMessage(OscMessage oscMessage, Action<string[], bool, object[]> callback)
        {
            if (string.IsNullOrEmpty(oscMessage.Address))
                return;


            var namespaces = oscMessage.Address.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            if(callback != null)
                callback(namespaces, namespaces[namespaces.Length-1] == "?", oscMessage.ToArray());
        }
    }
}
