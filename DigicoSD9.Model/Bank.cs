﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigicoSD9.Model
{
    public class Bank : BaseModel, IDisposable
    {
        private string _name;
        private string _side;

        public Bank()
        {
            BankFaders = new ItemsChangeObservableDictionary<int, BankFader>();
            for (int i = 1; i <= 12; i++)
            {
                BankFaders.Add(new KeyValuePair<int, BankFader>(i, new BankFader()));
            }
        }

        #region Properties
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public string Side
        {
            get { return _side; }
            set { SetProperty(ref _side, value); }
        }

        public ItemsChangeObservableDictionary<int, BankFader> BankFaders { get; private set; }
        #endregion

        #region IDisposable
        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~Bank()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (BankFaders != null)
                    BankFaders.Dispose();
            }
            // free native resources here if there are any
        } 
	    #endregion
    }
}
