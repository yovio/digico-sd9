﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers', [])

    // Path: /
    .controller('HomeCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA Template';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /about
    .controller('AboutCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | About';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /login
    .controller('LoginCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | Sign In';
        // TODO: Authorize a user
        $scope.login = function () {
            $location.path('/');
            return false;
        };
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }])

    // Path: /about
    .controller('AuxSendCtrl', ['$scope', '$location', '$window', 'Hub', function($scope, $location, $window, Hub) {
        $scope.$root.title = 'Digico SD9 | Aux Send from scope';
        $scope.$on('$viewContentLoaded', function() {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        //$("#testSlider").slider({ orientation: 'vertical', range: 'min', min: -100, max: 10, value: -30, step: 0.01 });
        $scope.AuxSend = {
                AuxNum : 8,
                InputChannels: [
                    { Name: "Piano", ChNum : 13, Value: null },
                    { Name: "Filler", ChNum: 15, Value: null },
                    { Name: "WL", ChNum: 36, Value: null },
                    { Name: "Pastor", ChNum: 37, Value: null }]};

        var onHubStateChange = function(change) {
            if (change.newState === $.signalR.connectionState.reconnecting) {
                console.log('Re-connecting');
            } else if (change.newState === $.signalR.connectionState.connected) {
                console.log('The server is online');
                for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
                    inputChHub.getAuxSendLevel($scope.AuxSend.InputChannels[i].ChNum, $scope.AuxSend.AuxNum);
                }
            }
        };
        var onAuxSendLevelMessage = function (channelNum, auxNum, value) {
            if (auxNum != $scope.AuxSend.AuxNum)
                return;

            for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
                if ($scope.AuxSend.InputChannels[i].ChNum == channelNum) {
                    $scope.$apply(function () {
                        $scope.AuxSend.InputChannels[i].lastSyncValue = value;
                        $scope.AuxSend.InputChannels[i].Value = value;                        
                    });
                }
            }
        };

        //declaring the hub connection
        var inputChHub = new Hub('inputChannelHub', {
            logging: true,
            transport: ['webSockets', 'longPolling'],
            rootPath: "http://localhost:8080/signalr",
            methods: ['getAuxSendLevel', 'setAuxSendLevel'],
            listeners: {
                'onAuxSendLevelMessage': onAuxSendLevelMessage
            },
            stateChanged: onHubStateChange,
            errorHandler: function(error) {
                console.error(error);
            }
        });

        //Send slider value every 100ms if changes
        var sendValue = function (auxNum, inputCh) {
            if (inputCh.Value && (!inputCh.lastSyncValue || Math.abs(inputCh.Value - inputCh.lastSyncValue) >= 0.1)) {
                inputCh.lastSyncValue = inputCh.Value;
                inputChHub.setAuxSendLevel(inputCh.ChNum, auxNum, inputCh.Value);
            }
        };

        setInterval(function () {
            if (inputChHub.connection.state == 1) {
                for (var i = 0; i < $scope.AuxSend.InputChannels.length; i++) {
                    sendValue($scope.AuxSend.AuxNum, $scope.AuxSend.InputChannels[i]);
                }
            }
            
        }, 50);

    }])

    // Path: /error/404
    .controller('Error404Ctrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'Error 404: Page Not Found';
        $scope.$on('$viewContentLoaded', function () {
            //We are not using Google Analytics so coment the below code
            //$window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);