﻿function AuxSendControl(name, container, min, max) {
    this.container = container;
    this.container.append("<h4 class='auxSendValLbl'></h4>");
    this.container.append("<div class=\"sliderDiv\"></div>");
    this.container.append("<h6 class='auxSendNameLbl'></h5>");
    this.valueLbl = this.container.children().first();
    this.sliderDiv = this.valueLbl.next();
    this.nameLbl = this.sliderDiv.next();
    this.container.addClass("auxSendDiv");

    if (min == null || min < -80)
        min = -80;
    if (max == null || max > 10)
        max = 10;

    this.sliderDiv.slider({
        disabled: true,
        orientation: "vertical",
        range: "min",
        step: 0.01,
        min: min,
        max: max
    });

    this.lastSyncValue = null;
    this.nameLbl.text(name);
    this.value = this.sliderDiv.slider("value");
    this.valueLbl.text(this.sliderDiv.slider("value") + " dB");
    
    this.getValue = function () {
        if (this.getSliderOption("disabled"))
            return null;
        else
            return this.sliderDiv.slider("value");
    };

    this.getSliderOption = function (optionName) {
        return this.sliderDiv.slider("option", optionName);
    };
    this.setSliderOption = function (optionName, value) {
        this.sliderDiv.slider("option", optionName, value);
    };

    this.setValue = function (val) {
        if (val == null)
            return;
        if (this.getSliderOption("disabled"))
            this.setSliderOption("disabled", false);

        this.lastSyncValue = val;
        this.sliderDiv.slider("value", val);
        this.valueLbl.text(val + " dB");
    };

    this.sliderDiv.on("slide", function (event, ui) {
        container.children("h4").text(ui.value + " dB");
    });
};