﻿'use strict';

angular.module('app.directives', [])
    .directive('appVersion', [
        'version', function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }
    ])

    .directive('auxSendControl', function () {
        return {
            restrict: 'E',
            template: "<div><span style=\"width:98%;display:inline-block;vertical-align: top;\" class=\"label label-info\">{{auxSend.Value}}dB</span>" +
                      "<div style=\"height:130px;margin-left:auto;margin-right:auto;margin-top:15px;margin-bottom:10px;text-align:center;vertical-align: middle;\"></div>" +
                      "<span style=\"width:98%;display:inline-block;vertical-align: bottom;\" class=\"label label-primary\">{{auxSend.Name}}</span></div>",
            scope: {
                option: "=option",
                auxSend: "="
            },
            replace: true,
            link: function (scope, elm, attrs) {
                var container = elm;
                var lblValue = container.children().first();
                var sliderDiv = lblValue.next();
                var nameLbl = sliderDiv.next();

                sliderDiv.slider(scope.option);

                sliderDiv.on("slide", function (event, ui) {
                    scope.$apply(function () {
                        scope.auxSend.Value = ui.value;
                    });
                });

                scope.$watch(
                    function (scope) { return scope.auxSend.Value },
                    function (newValue) {
                        sliderDiv.slider("value", newValue);
                        lblValue.text(newValue + " dB");
                    });
            }
    };
});