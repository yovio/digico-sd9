﻿namespace DigicoSD9.Web
{
    using System.Web;
    using System.Web.Optimization;
    public class BundleConfig
    {
        private const string jqueryVer = "1.11.2";
        private const string jqueryUIVer = "1.11.4";
        private const string signalrVer = "2.2.0";

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Styles
            bundles.Add(new StyleBundle("~/content/css/app").Include("~/content/app.css"));
            bundles.Add(new StyleBundle("~/content/css/jquery-ui").Include("~/content/jquery-ui/themes/delta/jquery-ui.css")); 
            #endregion

            #region Javascripts
            bundles.Add(new ScriptBundle("~/js/jquery").
                    Include(string.Format("~/scripts/vendor/jquery/jquery-{0}.min.js", jqueryVer)));

            bundles.Add(new ScriptBundle("~/js/jquery-ui").Include(string.Format("~/scripts/vendor/jquery-ui/jquery-ui-{0}.min.js", jqueryUIVer),
                "~/scripts/vendor/jquery-ui/jquery-ui.toggleSwitch.min.js",
                "~/scripts/vendor/jquery-ui/jquery.ui.touch-punch.min.js"));

            bundles.Add(new ScriptBundle("~/js/signalr").Include(string.Format("~/scripts/vendor/signalr/jquery.signalR-{0}.min.js", signalrVer)));

            bundles.Add(new ScriptBundle("~/js/angular").Include("~/scripts/vendor/angular/angular.min.js"));


            bundles.Add(new ScriptBundle("~/js/app").Include(
                "~/scripts/vendor/angular/angular-ui-router.min.js",
                "~/scripts/vendor/angular/angular-signalr-hub.js",
                "~/scripts/filters.js",
                "~/scripts/services.js",
                "~/scripts/directives.js",
                "~/scripts/controllers.js",
                "~/scripts/app.js"));
            #endregion
        }
    }
}