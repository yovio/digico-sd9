﻿
namespace DigicoSD9.Web
{
    using System.Web;
    using System.Web.Optimization;
    using System.Web.Routing;
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}