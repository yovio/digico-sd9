﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DigicoSD9.Model;
using DigicoSD9.Utility;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using NLog;
using Rug.Osc;

namespace DigicoSD9.Simulator
{
    public class DigicoSD9Server
    {
        private bool _isSD9ListenerStarted;
        private bool _isStopping;
        private CancellationTokenSource _serviceStopTokenSource = new CancellationTokenSource();
        private readonly ConcurrentQueue<Tuple<DateTime, IPEndPoint, byte[]>> _incomingMsgQueue = new ConcurrentQueue<Tuple<DateTime, IPEndPoint, byte[]>>();
        private readonly ConcurrentQueue<OscMessage> _outgoingMsgQueue = new ConcurrentQueue<OscMessage>();

        public DigicoSD9Server(IUnityContainer unityContainer)
        {
            this.UnityContainer = unityContainer;
            var consoleStatInJson = File.ReadAllText(@"SD9ConsoleStatus.json");
            SD9Console = JsonConvert.DeserializeObject<SD9Console>(consoleStatInJson);
            //SD9Console.SetConsoleStructure(8,48,6,8,12,8,16,1);

            //var test = JsonConvert.SerializeObject(SD9Console);
        }

        #region Properties
        public IUnityContainer UnityContainer { get; private set; }
        public int ListenPort { get; set; }
        public int SendPort { get; set; }
        public string ClientIP { get; set; }
        public SD9Console SD9Console { get; private set; }

        public ConcurrentQueue<OscMessage> OutgoingMsgQueue
        {
            get { return _outgoingMsgQueue; }
        }

        #endregion

        #region Public Methods
        public void Start()
        {
            UnityContainer.RegisterInstance(SD9Console);
            Task.Run((Action) ProcessOutgoingQueue);
            Task.Run((Action) ProcessIncomingQueue);
            Task.Run((Action) ListenIncomingPacket);
        }
        #endregion


        #region Private Methods

        private async void ListenIncomingPacket()
        {
            using (var udpListener = new UdpClient(ListenPort))
            {
                udpListener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("Digico SD9 Simulator Listener started and listening message coming on port {0}", ListenPort);

                _isSD9ListenerStarted = true;
                while (!_isStopping)
                {
                    try
                    {
                        var receivedResults = await udpListener.ReceiveAsync().WithCancellation(_serviceStopTokenSource.Token);
                        _incomingMsgQueue.Enqueue(new Tuple<DateTime, IPEndPoint, byte[]>(DateTime.Now, receivedResults.RemoteEndPoint, receivedResults.Buffer));
                    }
                    catch (OperationCanceledException ce)
                    {
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException("Exception happen when listening UDP Message", ex);
                    }
                }
                _isSD9ListenerStarted = false;
                logger.Info("Digico SD9 Simulator Listener stoped");
            }
        }
        
        private void ProcessIncomingQueue()
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("UDP Queue Processor started");
            while (!_isStopping)
            {
                if (_incomingMsgQueue.IsEmpty)
                {
                    Thread.Sleep(1);
                    continue;
                }

                Tuple<DateTime, IPEndPoint, byte[]> dequeuedMessage;
                while (_incomingMsgQueue.TryDequeue(out dequeuedMessage))
                {
                    try
                    {
                        var oscPacket = OscPacket.Read(dequeuedMessage.Item3, dequeuedMessage.Item3.Length, dequeuedMessage.Item2);
                        oscPacket.ParseOscPacket(PacketParserCallback);
                        
                        logger.Debug("Incoming UDP Message as OSC : {0}\r\n", oscPacket);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("Unable to parse incoming UDP Message as OSC. Original Message in Base64 : {0}\r\n", Convert.ToBase64String(dequeuedMessage.Item3));
                    }
                }
            }
            logger.Info("UDP Queue Processor stoped");
        }

        private void ProcessOutgoingQueue()
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("Outgoing Message Queue Processor started");

            using (UdpClient sender = new UdpClient())
            {
                sender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                sender.Connect(ClientIP, SendPort);
                while (!_isStopping)
                {
                    if (OutgoingMsgQueue.IsEmpty)
                    {
                        Thread.Sleep(1);
                        continue;
                    }

                    OscMessage dequeuedOsc;
                    while (OutgoingMsgQueue.TryDequeue(out dequeuedOsc))
                    {
                        try
                        {
                            OscMessage peekedOscMessage;
                            if (OutgoingMsgQueue.TryPeek(out peekedOscMessage))
                            {
                                if (string.Equals(peekedOscMessage.Address, dequeuedOsc.Address))
                                    //Next message is having same address, so skip and send the next instead
                                {
                                    logger.Debug("Skip Send OSC Message {0} to {1}:{2}", dequeuedOsc, ClientIP, SendPort);
                                    continue;
                                }
                            }
                            var bytes = dequeuedOsc.ToByteArray();
                            sender.Send(bytes, bytes.Length);
                            logger.Debug("Send OSC Message {0} to {1}:{2}", dequeuedOsc, ClientIP, SendPort);
                            Thread.Sleep(5);
                        }
                        catch (Exception)
                        {
                            logger.Warn("Failed sending OSC Message to SD9: {0}", dequeuedOsc);
                        }
                    }
                }
                sender.Close();
                logger.Info("Outgoing Message Queue Processor stoped");
            }
        }

        private void PacketParserCallback(string[] namespaces, bool isQuery, object[] arguments)
        {
            switch (namespaces[0])
            {
                case "Input_Channels":
                    ProcessInputChannels(namespaces, isQuery, arguments);
                    break;
                case "Console":
                    ProcessConsole(namespaces, isQuery, arguments);
                    break;
                case "Aux_Outputs":
                    ProcessAuxOutput(namespaces, isQuery, arguments);
                    break;
                case "Control_Groups":
                    ProcessControlGroups(namespaces, isQuery, arguments);
                    break;
                case "Group_Outputs":
                    ProcessGroupOutputs(namespaces, isQuery, arguments);
                    break;
                case "Matrix_Outputs":
                    ProcessMatrixOutputs(namespaces, isQuery, arguments);
                    break;
            }
        }

        private void ProcessMatrixOutputs(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte matrixOutNo = byte.Parse(namespaces[1]);
            var selMatrixOut = SD9Console.MatrixOuts[matrixOutNo];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                    {
                        if (namespaces[3] == "name")
                        {
                            if (isQuery)
                            {
                                if (selMatrixOut.Name != null)
                                {
                                    var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)), selMatrixOut.Name);
                                    OutgoingMsgQueue.Enqueue(outMessage);
                                }
                            }
                            else
                            {
                                selMatrixOut.Name = arguments[0] as string;
                            }
                        }
                    }
                    break;
            }
        }

        private void ProcessGroupOutputs(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte groupNo = byte.Parse(namespaces[1]);
            var selectedGroupOut = SD9Console.GroupOutputs[groupNo];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                    {
                        if (namespaces[3] == "name")
                        {
                            if (isQuery)
                            {
                                if (selectedGroupOut.Name != null)
                                {
                                    var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)), selectedGroupOut.Name);
                                    OutgoingMsgQueue.Enqueue(outMessage);
                                }
                            }
                            else
                            {
                                selectedGroupOut.Name = arguments[0] as string;
                            }
                        }
                    }
                    break;
            }
        }

        private void ProcessControlGroups(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte groupNo = byte.Parse(namespaces[1]);
            var selControlGroup = SD9Console.ControlGroups[groupNo];
            switch (namespaces[2])
            {
                case "name":
                    {
                        if (isQuery)
                        {
                            if (selControlGroup.Name != null)
                            {
                                var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)), selControlGroup.Name);
                                OutgoingMsgQueue.Enqueue(outMessage);
                            }
                        }
                        else
                        {
                            selControlGroup.Name = arguments[0] as string;
                        }
                        
                    }
                    break;
            }
        }

        private void ProcessAuxOutput(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte auxNum = byte.Parse(namespaces[1]);
            var selectedAuxOut = SD9Console.AuxOutputs[auxNum];
            switch (namespaces[2])
            {
                case "Buss_Trim":
                {
                    if (namespaces[3] == "name")
                    {
                        if (isQuery)
                        {
                            var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)), selectedAuxOut.Name);
                            OutgoingMsgQueue.Enqueue(outMessage);
                        }
                        else
                        {
                            selectedAuxOut.Name = arguments[0] as string;
                        }
                    }
                }
                break;
            }
        }

        private void ProcessConsole(string[] namespaces, bool isQuery, object[] arguments)
        {
            switch (namespaces[1])
            {
                case "Name":
                    if (isQuery)
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Name", "SD9"));
                    break;
                case "Channels":
                    if (isQuery)
                    {
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Input_Channels", SD9Console.InputChannels.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Aux_Outputs", SD9Console.AuxOutputs.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Group_Outputs", SD9Console.GroupOutputs.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Talkback_Outputs", SD9Console.TalkbackOuts.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Control_Groups", SD9Console.ControlGroups.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Matrix_Inputs", SD9Console.MatrixIns.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Matrix_Outputs", SD9Console.MatrixOuts.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Graphic_EQ", SD9Console.GraphicEQs.Count));
                        OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Multis", 0));
                    }
                    break;
                case "Aux_Outputs":
                    if (namespaces[2] == "modes")
                    {
                        if (isQuery)
                        {
                            var args = SD9Console.AuxOutputs.Select(ao => (object)(int)ao.Value.LineMode).ToArray();
                            OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Aux_Outputs/modes", args));
                        }
                    }
                    break;
                case "Input_Channels":
                    if (namespaces[2] == "modes")
                    {
                        if (isQuery)
                        {
                            var args = SD9Console.InputChannels.Select(ic => (object)(int)ic.Value.LineMode).ToArray();
                            OutgoingMsgQueue.Enqueue(new OscMessage("/Console/Input_Channels/modes", args));
                        }
                    }
                    break;
            }
        }

        private void ProcessInputChannels(string[] namespaces, bool isQuery, object[] arguments)
        {
            byte chNum = byte.Parse(namespaces[1]);
            var selectedChannel = SD9Console.GetInputChannel(chNum);
            switch (namespaces[2])
            {
                case "Aux_Send":
                {
                    var auxNum = byte.Parse(namespaces[3]);
                    var selAuxSend = selectedChannel.GetAuxSend(auxNum);

                    if (string.Equals(namespaces[4], "send_level", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (isQuery)
                        {
                            var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)),
                                selAuxSend.Level);
                            OutgoingMsgQueue.Enqueue(outMessage);
                        }
                        else
                        {
                            selAuxSend.Level = (float) arguments[0];
                        }
                    }
                    else if (string.Equals(namespaces[4], "send_on", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (isQuery)
                        {
                            var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)),
                                selAuxSend.Enabled?1f:0f);
                            OutgoingMsgQueue.Enqueue(outMessage);
                        }
                        else
                        {
                            selAuxSend.Enabled = Math.Abs((float)arguments[0] - 1) < 0.1f;
                        }
                    }
                }
                    break;
                case "Channel_Input":
                {
                    if (namespaces[3] == "name")
                    {
                        if (isQuery)
                        {
                            var outMessage = new OscMessage("/" + string.Join("/", namespaces.Take(namespaces.Length - 1)), selectedChannel.Name);
                            OutgoingMsgQueue.Enqueue(outMessage);
                        }
                        else
                        {
                            selectedChannel.Name = arguments[0] as string;
                        }
                    }
                }
                    break;
            }
        }
        #endregion
    }
}
