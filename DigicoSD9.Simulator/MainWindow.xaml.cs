﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigicoSD9.Model;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using NLog;
using Rug.Osc;

namespace DigicoSD9.Simulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Logger _logger = LogManager.GetCurrentClassLogger();

        public MainWindow()
        {
            _logger.Info("Starting Digico SD9 Simulator");
            this.UnityContainer = ConstructUnityContainer();
            InitializeComponent();

            DigicoSd9Server.Start();
            this.DataContext = SD9Console;
        }

        public IUnityContainer UnityContainer { get; set; }

        public SD9Console SD9Console
        {
            get { return UnityContainer.Resolve<SD9Console>(); }
        }

        public DigicoSD9Server DigicoSd9Server
        {
            get { return UnityContainer.Resolve<DigicoSD9Server>(); }
        }

        #region Private Methods
        private static UnityContainer ConstructUnityContainer()
        {
            var unityContainer = new UnityContainer();

            var configSection = ConfigurationManager.GetSection("unity") as UnityConfigurationSection;
            configSection.Configure(unityContainer);

            return unityContainer;
        } 
        #endregion

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Math.Abs(e.OldValue - e.NewValue) > 0.1f)
            {
                DigicoSd9Server.OutgoingMsgQueue.Enqueue(new OscMessage("/Input_Channels/13/Aux_Send/2/send_level", (float)sld1.Value));
            }
        }

        private void btnOnOff_Click(object sender, RoutedEventArgs e)
        {
            SD9Console.InputChannels[13].AuxSends[2].Enabled = !SD9Console.InputChannels[13].AuxSends[2].Enabled;
            DigicoSd9Server.OutgoingMsgQueue.Enqueue(new OscMessage("/Input_Channels/13/Aux_Send/2/send_on", SD9Console.InputChannels[13].AuxSends[2].Enabled ? 1f : 0f));
        }
    }
}
