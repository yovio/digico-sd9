﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Rug.Osc;

namespace UdpProxy
{
    class Program
    {
        private const byte MESAGE_FROM_SD9 = 0;
        private const byte MESAGE_FROM_IPAD = 1;

        private static NLog.Logger _logger;
        private static ConcurrentQueue<Tuple<DateTime, byte, IPEndPoint, byte[]>> _messagesQueue = new ConcurrentQueue<Tuple<DateTime, byte, IPEndPoint, byte[]>>();

        static void Main(string[] args)
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();

            var sd9IP = IPAddress.Parse(args[0]);
            var ipadIP = IPAddress.Parse(args[1]);


            StartSD9Proxy(sd9IP, ipadIP);

            Console.ReadLine();
        }

        private static void StartSD9Proxy(IPAddress sd9IP, IPAddress ipadIP, int sd9SendPort = 8000,
            int sd9ReceivePort = 9000, int ipadReceivePort = 8001, int ipadSendPort = 9001)
        {
            _logger.Info("Starting Proxy.");

            Task.Run((Action) ProcessQueue); 

            #region Task Listening send message from SD9
            Task.Run(async () =>
                    {
                        using (var udpSender = new UdpClient())
                        {
                            using (var udpListener = new UdpClient(sd9SendPort))
                            {
                                var sd9SendMsgForwardEP = new IPEndPoint(ipadIP, ipadReceivePort);
                                while (true)
                                {
                                    var receivedResults = await udpListener.ReceiveAsync();
                                    udpSender.Send(receivedResults.Buffer, receivedResults.Buffer.Length, sd9SendMsgForwardEP);
                                    _messagesQueue.Enqueue(new Tuple<DateTime, byte, IPEndPoint, byte[]>(DateTime.Now, MESAGE_FROM_SD9, receivedResults.RemoteEndPoint, receivedResults.Buffer));
                                }
                            }
                        }
                    }); 
            #endregion

            #region Task Listening send message from Ipad
            Task.Run(async () =>
            {
                using (var udpSender = new UdpClient())
                {
                    using (var udpListener = new UdpClient(ipadSendPort))
                    {
                        var ipadSendMsgForwardEP = new IPEndPoint(sd9IP, sd9ReceivePort);
                        
                        while (true)
                        {
                            var receivedResults = await udpListener.ReceiveAsync();
                            udpSender.Send(receivedResults.Buffer, receivedResults.Buffer.Length, ipadSendMsgForwardEP);
                            _messagesQueue.Enqueue(new Tuple<DateTime, byte, IPEndPoint, byte[]>(DateTime.Now, MESAGE_FROM_IPAD, receivedResults.RemoteEndPoint, receivedResults.Buffer));
                        }
                    }
                }
            });
            #endregion
        }

        private static void ProcessMessage(Tuple<DateTime, byte, IPEndPoint, byte[]> dequeuedMessage)
        {
            var loggingEvent = Convert.ToBase64String(dequeuedMessage.Item4);
            var msgOrigin = dequeuedMessage.Item2 == MESAGE_FROM_SD9 ? "SD9" : "IPAD";
            _logger.Info("{0}:  Raw Message in Base64 Encoding: {1}", msgOrigin, loggingEvent);
            try
            {
                var oscPacket = OscPacket.Read(dequeuedMessage.Item4, dequeuedMessage.Item4.Length, dequeuedMessage.Item3);
                _logger.Info("{0}:  Message in OSC : {1}\r\n", msgOrigin, oscPacket);
            }
            catch (Exception)
            {
                _logger.Warn("{0}:  Unable to parse Message as OSC\r\n", msgOrigin);
            }
        }

        private void ProcessQueue()
        {
            while (true)
            {
                if (_messagesQueue.IsEmpty)
                    continue;

                Tuple<DateTime, byte, IPEndPoint, byte[]> dequeuedMessage;
                while (_messagesQueue.TryDequeue(out dequeuedMessage))
                {
                    ProcessMessage(dequeuedMessage);
                }
            }
        }
    }
}
