﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Rug.Osc;

namespace OscClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputString;
            // This is the ip address we are going to send to
            //IPAddress address = IPAddress.Parse("10.91.120.60");
            var address = "127.0.0.1";
            // This is the port we are going to send to 
            int port = 8000;
            do
            {
                Console.Write("Press s to send another message, other key to exit");

                // Create a new sender instance
                using (UdpClient sender = new UdpClient(address, port))
                {
                    sender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                    //var msg = new OscMessage("/Console/Name", "SD9");
                    var msg = new OscMessage("/Input_Channels/2/Aux_Send/1/send_level", -10f);
                    var bytes = msg.ToByteArray();
                    // Send a new message
                    sender.Send(bytes,bytes.Length);
                }
                
                inputString = Console.ReadLine();
            }
            while(string.Equals(inputString, "s", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
