﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Rug.Osc;

namespace DigicoSD9.QueryTools
{
    class Program
    {
        private const int C_MilisBeforeNextQuery = 2000;
        private const int C_listenPort = 8000;
        private const int C_sendPort = 9000;
        private const string C_SD9IP = "192.168.6.129";

        private static bool _isStopping;
        private static DateTime _lastReceiveMessage;

        static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            Task.Run((Action)ListenIncomingPacket);

            using (UdpClient sender = new UdpClient())
            {
                sender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                sender.Connect(C_SD9IP, C_sendPort);
                
                using (var fi = File.OpenText("QueryFile.txt"))
                {
                    while (!fi.EndOfStream)
                    {
                        var lineRead = fi.ReadLine().Trim();
                        var oscMsg = new OscMessage(lineRead);
                        try
                        {
                            var bytes = oscMsg.ToByteArray();
                            sender.Send(bytes, bytes.Length);
                            _lastReceiveMessage = DateTime.Now;
                        }
                        catch (Exception)
                        {
                            logger.Warn("Failed sending OSC Message to SD9: {0}", oscMsg);
                        }

                        logger.Info("To SD9: {0}", lineRead);
                        Thread.Sleep(5);

                        while (true)
                        {
                            if ((DateTime.Now - _lastReceiveMessage).TotalMilliseconds < C_MilisBeforeNextQuery)
                                Thread.Sleep(100);
                            else
                            {
                                break;
                            }
                        }

                    }
                }
                
                sender.Close();
            }
            Console.WriteLine("Finish, press any key to exit.");
            Console.ReadLine();

        }

        private static async void ListenIncomingPacket()
        {
            using (var udpListener = new UdpClient(C_listenPort))
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("Digico SD9 Query Tools Listener started and listening message coming on port {0}",
                    C_listenPort);


                while (!_isStopping)
                {
                    try
                    {
                        var receivedResults = await udpListener.ReceiveAsync();
                        _lastReceiveMessage = DateTime.Now;
                        var oscPacket = OscPacket.Read(receivedResults.Buffer, receivedResults.Buffer.Length, receivedResults.RemoteEndPoint);
                        logger.Info("From SD9: {0}", oscPacket);
                    }
                    catch (OperationCanceledException ce)
                    {
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException("Exception happen when listening UDP Message", ex);
                    }
                }
                logger.Info("Digico SD9 Simulator Listener stoped");
            }


        }
    }
}
