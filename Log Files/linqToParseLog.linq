<Query Kind="Program">
  <NuGetReference>Rug.Osc</NuGetReference>
  <Namespace>Rug.Osc</Namespace>
</Query>

void Main()
{
	var oscMessagesFromSD9 = new List<OscMessage>();
	var oscMessagesFromIpad = new List<OscMessage>();	
	
	Path.GetDirectoryName(Util.CurrentQueryPath).Dump();
	
	var fs = File.OpenText(Path.GetDirectoryName(Util.CurrentQueryPath) + @"\CombineLog.txt");
	parseFile(fs, oscMessagesFromSD9, oscMessagesFromIpad);
	
	oscMessagesFromIpad.Sort();
	oscMessagesFromIpad.GroupBy(msg=>msg.NameSpace).Select(grp => grp.First()).ToList().Dump();
	
	oscMessagesFromSD9.Sort();
	oscMessagesFromSD9.Dump();
}

void parseFile(StreamReader fs, List<OscMessage> oscMessagesFromSD9, List<OscMessage> oscMessagesFromIpad)
{
	while(!fs.EndOfStream)
	{
		var lineRead = fs.ReadLine();
		var oscIdx = lineRead.IndexOf("Message in OSC");
		if(oscIdx > -1)
		{
			var time = DateTime.Parse(lineRead.Substring(0,24));
			bool fromSD9 = lineRead.Contains("SD9:");
			var oscNameSpaceEndIdx = lineRead.IndexOf(", ", oscIdx);
			string oscNameSpace = null;
			OscMessage oscMessage = null;
			if(oscNameSpaceEndIdx > -1)
			{
				oscNameSpace = lineRead.Substring(oscIdx + 17, oscNameSpaceEndIdx - 17 - oscIdx);
				var oscValues = lineRead.Substring(oscNameSpaceEndIdx);
				
				
				oscMessage = new OscMessage(){
					Time = time,
					FromSD9 = fromSD9,
					NameSpace = oscNameSpace,
					Value = oscValues
				};
				
				//oscMessage.Dump();
			}
			else if(lineRead.EndsWith("?"))
			{
				oscNameSpace = lineRead.Substring(oscIdx + 17);
				oscMessage = new OscMessage(){
					Time = time,
					FromSD9 = fromSD9,
					NameSpace = oscNameSpace
				};
			}
			
			if(oscMessage != null)
			{
				if(fromSD9 && !oscMessagesFromSD9.Contains(oscMessage))
					oscMessagesFromSD9.Add(oscMessage);
				else if(!fromSD9 &&!oscMessagesFromIpad.Contains(oscMessage))
					oscMessagesFromIpad.Add(oscMessage);
			}				
		}
	}
	fs.Close();
}

public class OscMessage : IComparable
{
	public DateTime Time { get; set; }
	public bool FromSD9 { get; set; }
	public string NameSpace { get; set; }
	public string Value { get; set; }
	
	public int CompareTo(object obj) {
        if (obj == null) return 1;

        OscMessage otherMessage = obj as OscMessage;
        if (otherMessage != null) 
            return this.NameSpace.CompareTo(otherMessage.NameSpace);
        else 
           throw new ArgumentException("Object is not a OscMessage");
    }
}
// Define other methods and classes here